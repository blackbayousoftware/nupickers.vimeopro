* Version 1.0.0 - Initial Version
* Version 1.0.1 - Updated targeted .NET Framework Version to 4.5. Previous version targeted 4.5.1
* Version 1.1.0 - Added sort and license filtering parameters