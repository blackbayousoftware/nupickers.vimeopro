nuPickers-VimeoPickers
=======================

__Download:__ [NuGet Package](https://www.nuget.org/packages/nuPickers-VimeoPicker/), [Umbraco Package](https://our.umbraco.org/projects/backoffice-extensions/nupickers-vimeo-picker/)

__Support:__ [Documentation Wiki](https://bitbucket.org/blackbayousoftware/nupickers.vimeopro/wiki/Home), [Issue Logging](https://bitbucket.org/blackbayousoftware/nupickers.vimeopro/issues?status=new&status=open)

__Credits:__ [Hendy Rancher - nuPickers-FlickerPickers](https://github.com/Hendy/nuPickers-FlickrPickers) Inspiration for this package came from the Flickr Picker package.