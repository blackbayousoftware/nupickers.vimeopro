﻿namespace nuPickers.VimeoPro.DataSources
{
    using nuPickers.Shared.DotNetDataSource;
    using nuPickers.VimeoPro.Model;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Implementing IDotNetDataSource, enables this class to be used with any "nuPicker: DotNet ... Picker"
    /// Implementing IDotNetDataSourceTypeahead, enables this class to recieve any typeahead text from a "nuPicker DotNet TypeaheadList Picker"
    /// </summary>
    public class VimeoVideoDataSource : IDotNetDataSource, IDotNetDataSourceTypeahead
    {

        [DotNetDataSource(Title = "Vimeo Auth Key", Description = "(required)")]
        public string AuthKey { get; set; }

        [DotNetDataSource(Title = "User Id", Description = "Optional - The id of the user used when searching for videos")]
        public string UserId { get; set; }

        [DotNetDataSource(Title = "Search Text", Description = "This search query used when searching for videos in non-typeahead pickers.")]
        public string SearchText { get; set; }

        [DotNetDataSource(Title = "Result Sort Order", Description = "Allowed values: relevant, date, alphabetical, plays, likes, comments, and duration")]
        public string SortBy { get; set; }

        [DotNetDataSource(Title = "Result Sort Direction", Description = "Allowed values: asc and desc")]
        public string SortDirection { get; set; }

        [DotNetDataSource(Title = "Rating Filter", Description = "Filter results by the following ratings: CC, CC-BY, CC-BY-SA, CC-BY-ND, CC-BY-NC, CC-BY-NC-SA, CC-BY-NC-ND, or in-progress")]
        public string LicenseFilter { get; set; }

        [DotNetDataSource(Title = "Thumbnail Height" , Description = "Size of the thumbnail height used for the default image in the picker.")]
        public int ThumbnailHeight { get; set; }

        [DotNetDataSource(Title = "Page Size", Description = "How many results should be included in the search")]
        public int PageSize { get; set; }

        public VimeoVideoDataSource()
        {
            ThumbnailHeight = 150;
            PageSize = 30;
            SortBy = "alphabetical";
            SortDirection = "asc"; 
        }

        /// <summary>
        /// The current typeahead text (this is only set if using a DotNet TypeaheadList Picker)
        /// </summary>
        string IDotNetDataSourceTypeahead.Typeahead { get; set; }

        /// <summary>
        /// Helper to get at the Typeahead value
        /// </summary>
        private string Typeahead
        {
            get
            {
                return ((IDotNetDataSourceTypeahead)this).Typeahead;
            }
        }

        /// <summary>
        /// This is the main method called from the DotNetDataSource
        /// </summary>
        /// <returns>a collection of key / labels for the picker</returns>
        public IEnumerable<KeyValuePair<string, string>> GetEditorDataItems(int contextId)
        {
            // begin query
            VideoSearchOptions searchOptions = new VideoSearchOptions();
            searchOptions.UserId = this.UserId;
            searchOptions.PageSize = this.PageSize;
            searchOptions.SortBy = this.SortBy; 
            searchOptions.SortDirection = this.SortDirection;
            searchOptions.LicenseFilter = this.LicenseFilter; 

            // if being used by a typeahead picker...
            if (!string.IsNullOrWhiteSpace(this.Typeahead))
            {
                searchOptions.Text = this.Typeahead;
            }else
            {
                searchOptions.Text = this.SearchText; 
            }  

            return VimeoManager
                .GetVimeoConnection(this.AuthKey)
                .GetVideos(searchOptions)
                .Select(x => new KeyValuePair<string, string>(
                    x.VideoId.ToString(), 
                    string.Format(
                        @"  <h3>{0}</h3>
                            <p>Video Id: {1}</p>
                            <a href='{2}' target='_blank'><img src='{3}' /></a>", 
                        x.Name,
                        x.VideoId,
                        x.Link,
                        x.Pictures.Select(p => p.Sizes.Where(s => s.Height == this.ThumbnailHeight).Select(s => s.Link).FirstOrDefault()).FirstOrDefault()
                        )
                    )
                );
        }

    }
}