﻿namespace nuPickers.VimeoPro
{
    using nuPickers.VimeoPro.Authentication;
    using nuPickers.VimeoPro.Model;
    using RestSharp;
    using RestSharp.Deserializers;
    using System;
    using System.Collections.Generic;

    public class VimeoService
    {
        private readonly RestClient apiClient;

        public VimeoService(string accessToken)
        {
            apiClient = new RestClient("https://api.vimeo.com");
            apiClient.Authenticator = new VimeoAuthenticator(accessToken);
            apiClient.AddHandler("application/vnd.vimeo.video+json", new JsonDeserializer());
        }

        public List<VimeoVideo> SearchVideos(VideoSearchOptions searchOptions)
        {
            if (searchOptions == null) throw new ArgumentNullException("searchOptions");
            if (string.IsNullOrEmpty(searchOptions.Text)) throw new ArgumentNullException("searchOptions.Text");

            var requestUrl = string.IsNullOrEmpty(searchOptions.UserId) ?
                "videos" :
                "users/{userId}/videos";
            var request = new RestRequest(requestUrl, Method.GET);
            request.RequestFormat = DataFormat.Json;

            if (!string.IsNullOrEmpty(searchOptions.UserId))
            {
                request.AddUrlSegment("userId", searchOptions.UserId);
            }
            
            request.AddParameter("query", searchOptions.Text);
            request.AddParameter("page", searchOptions.Page);
            request.AddParameter("per_page", searchOptions.PageSize);
            
            if (!string.IsNullOrEmpty(searchOptions.SortBy))
            {
                request.AddParameter("sort", searchOptions.SortBy); 
            }

            if (!string.IsNullOrEmpty(searchOptions.SortDirection))
            {
                request.AddParameter("direction", searchOptions.SortDirection); 
            }

            if (!string.IsNullOrEmpty(searchOptions.LicenseFilter))
            {
                request.AddParameter("filter", searchOptions.LicenseFilter); 
            }


            // TODO - Should we handle more than one page, or just let the user enter more specific terms?
            var response = apiClient.Execute<ApiResponse<List<VimeoVideo>>>(request);
            return response.Data.Data;
        }

        public VimeoVideo GetVideo(long clipId)
        {
            var request = new RestRequest("videos/{videoId}", Method.GET);
            request.AddUrlSegment("videoId", clipId.ToString());

            var response = apiClient.Execute<VimeoVideo>(request);
            return response.Data;
        }

        public VideoOEmbed GetVideoOEmbedData(VideoOEmbedRequestSettings requestSettings)
        {
            var oEmbedClient = new RestClient("https://vimeo.com/api/oembed.json"); 
            
            var oEmbedRequest = new RestRequest();
            oEmbedRequest.AddParameter("url", string.Format("https://vimeo.com/{0}", requestSettings.VideoId));
            oEmbedRequest.AddParameter("width", requestSettings.Width);
            oEmbedRequest.AddParameter("maxwidth", requestSettings.MaxWidth);
            oEmbedRequest.AddParameter("height", requestSettings.Height);
            oEmbedRequest.AddParameter("maxheight", requestSettings.MaxHeight);
            oEmbedRequest.AddParameter("byline", requestSettings.Byline);
            oEmbedRequest.AddParameter("title", requestSettings.Title);
            oEmbedRequest.AddParameter("portrait", requestSettings.Portrait);
            oEmbedRequest.AddParameter("color", requestSettings.Color);
            oEmbedRequest.AddParameter("autoplay", requestSettings.AutoPlay);
            oEmbedRequest.AddParameter("xhtml", requestSettings.XHtml);
            oEmbedRequest.AddParameter("api", requestSettings.ApiEnabled);
            oEmbedRequest.AddParameter("player_id", requestSettings.PlayerId); 

            var response = oEmbedClient.Execute<VideoOEmbed>(oEmbedRequest);

            return response.Data; 
        }
    }
}
