﻿namespace nuPickers.VimeoPro
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    internal sealed class VimeoManager
    {
        private static readonly Lazy<VimeoManager> lazy = new Lazy<VimeoManager>(() => new VimeoManager());

        private static readonly object lockObject = new object();

        /// <summary>
        /// Collection of VimeoConnections where each represents a unique key / secret combination
        /// </summary>
        private List<VimeoConnection> VimeoConnections { get; set; }


        private VimeoManager()
        {
            this.VimeoConnections = new List<VimeoConnection>();
        }

        /// <summary>
        /// Attempts to add connection to the collection
        /// </summary>
        /// <param name="authToken"></param>>
        private void AddVimeoConnection(string authToken)
        {
            // prevent race condition that could create multiple identical connections
            lock (lockObject)
            {
                // check again - another thread might have beaten us here...
                if (!this.VimeoConnections.Any(x => x.AuthToken == authToken))
                {
                    this.VimeoConnections.Add(new VimeoConnection(authToken));
                }
            }
        }

        /// <summary>
        /// returns the same instance of the VimeoConnection obj for the specified parameters
        /// </summary>
        /// <param name="authToken"></param>
        /// <returns></returns>
        internal static VimeoConnection GetVimeoConnection(string authToken)
        {
            // get the singleton instance of VimeoManager
            VimeoManager manager = lazy.Value;

            // try and get an existing connection
            VimeoConnection connection = manager.VimeoConnections.SingleOrDefault(x => x.AuthToken == authToken);

            // if connection not found
            if (connection == null)
            {
                // ensure new connection is / has been added
                manager.AddVimeoConnection(authToken);

                // expect to find connection
                connection = manager.VimeoConnections.Single(x => x.AuthToken == authToken);
            }

            return connection;
        }
    }
}