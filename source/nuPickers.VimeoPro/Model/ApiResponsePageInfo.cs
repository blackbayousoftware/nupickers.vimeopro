﻿namespace nuPickers.VimeoPro.Model
{
    public class ApiResponsePageInfo
    {
        public string Next { get; set; }
        public string Previous { get; set; }
        public string First { get; set; }
        public string Last { get; set; }
    }
}
