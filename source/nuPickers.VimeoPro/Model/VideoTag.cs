﻿namespace nuPickers.VimeoPro.Model
{
    public class VideoTag
    {
        public string Uri { get; set; }
        public string Name { get; set; }
        public string Tag { get; set; }
    }
}
