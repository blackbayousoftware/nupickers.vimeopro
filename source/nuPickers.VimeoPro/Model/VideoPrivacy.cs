﻿namespace nuPickers.VimeoPro.Model
{
    public class VideoPrivacy
    {
        public string View { get; set; }

        public string Embed { get; set; }

        public bool Download { get; set; }

        public bool Add { get; set; }

        public string Comments { get; set; }
    }
}
