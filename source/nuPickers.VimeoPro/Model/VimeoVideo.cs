﻿namespace nuPickers.VimeoPro.Model
{
    using RestSharp.Deserializers;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class VimeoVideo
    {

        public string Uri { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Link { get; set; }

        public int Duration { get; set; }

        public int Width { get; set; }

        public string Language { get; set; }

        public int Height { get; set; }

        public VideoEmbed Embed { get; set; }

        public DateTime CreatedTime { get; set; }

        public DateTime ModifiedTime { get; set; }

        public List<string> ContentRating { get; set; }

        public string License { get; set; }

        public VideoPrivacy Privacy { get; set; }

        public List<Picture> Pictures { get; set; }

        public List<VideoTag> Tags { get; set; }

        public VideoStats Stats { get; set; }

        public string Status { get; set; }

        public long VideoId
        {
            get
            {
                if (!string.IsNullOrEmpty(Uri))
                {
                    var id = Uri.Substring(Uri.LastIndexOf("/")+1);
                    long idValue;
                    if (long.TryParse(id, out idValue))
                    {
                        return idValue; 
                    }
                }

                return -1; 
            }
        }

        public VimeoVideo()
        {
            this.Pictures = new List<Picture>();
            this.Tags = new List<VideoTag>();
        }
    }
}
