﻿namespace nuPickers.VimeoPro.Model
{
    using System.Collections.Generic;

    public class Picture
    {
        public bool Active { get; set; }
        public string Type { get; set; }
        public List<Size> Sizes { get; set; }
        public string Uri { get; set; }
    }
}
