﻿namespace nuPickers.VimeoPro.Model
{
    public class VideoOEmbed
    {
        public string Type { get; set; }
        public string Version { get; set; }
        public string ProviderName { get; set; }
        public string ProviderUrl { get; set; }
        public string Title { get; set; }
        public string AuthorName { get; set; }
        public string AuthorUrl { get; set; }
        public string IsPlus { get; set; }
        public string Html { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int Duration { get; set; }
        public string Description { get; set; }
        public string THumbnailUrl { get; set; }
        public int ThumbnailWidth { get; set; }
        public int ThumbnailHeight { get; set; }
        public int VideoId { get; set; }
        public string Uri { get; set; }
    }
}