﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nuPickers.VimeoPro.Model
{
    public class ApiResponse<T>
    {
        public int Total { get; set; }
        public int Page { get; set; }
        public int PerPage { get; set; }
        public ApiResponsePageInfo Paging { get; set; }
        public T Data { get; set; }
    }
}
