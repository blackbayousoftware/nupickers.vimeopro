﻿namespace nuPickers.VimeoPro.Model
{
    public class VideoOEmbedRequestSettings
    {
        public long VideoId { get; set; }
        public int Width { get; set; }
        public int MaxWidth { get; set; }
        public int Height { get; set; }
        public int MaxHeight { get; set; }
        public bool Byline { get; set; }
        public bool Title { get; set; }
        public bool Portrait { get; set; }
        public string Color { get; set; }
        public bool AutoPlay { get; set; }
        public bool XHtml { get; set; }
        public bool ApiEnabled { get; set; }
        public string PlayerId { get; set; }

        public VideoOEmbedRequestSettings(long videoId)
        {
            this.VideoId = videoId;
            this.Width = 640;
            this.MaxWidth = -1;
            this.Height = 400;
            this.MaxHeight = -1;
            this.Byline = true;
            this.Title = true;
            this.Portrait = true;
            this.XHtml = true; 
        }
    }
}
