﻿namespace nuPickers.VimeoPro.Model
{
    public class VideoSearchOptions
    {
        public string Text { get; set; }

        public string UserId { get; set; }

        public int Page { get; set; }

        public int PageSize { get; set; }

        public VideoSearchOptions()
        {
            Page = 1; 
            PageSize = 30; 
        }

        public string LicenseFilter { get; set; }

        public string SortDirection { get; set; }

        public string SortBy { get; set; }
    }
}
