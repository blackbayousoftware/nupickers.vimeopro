﻿namespace nuPickers.VimeoPro.Model
{
    public class Size
    {
        public int Height { get; set; }
        public int Width { get; set; }
        public string Link { get; set; }
    }
}
