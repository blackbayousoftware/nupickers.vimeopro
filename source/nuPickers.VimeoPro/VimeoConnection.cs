﻿namespace nuPickers.VimeoPro
{
    using nuPickers.VimeoPro.Model;
    using System.Collections.Generic;
    using System.Runtime.Caching;

    internal sealed class VimeoConnection
    {
        internal string AuthToken { get; private set; }

        private VimeoService Vimeo { get; set; }

        private MemoryCache MemoryCache { get; set; }

        internal VimeoConnection(string authToken)
        {
            this.AuthToken = authToken;
            this.Vimeo = new VimeoService(this.AuthToken);

            // scope cache to this connection only
            this.MemoryCache = new MemoryCache(this.AuthToken);
        }

   
        internal IEnumerable<VimeoVideo> GetVideos(VideoSearchOptions searchOptions)
        {
            List<VimeoVideo> results = new List<VimeoVideo>();

            try
            {
                results = this.Vimeo.SearchVideos(searchOptions);
                results.ForEach(v => this.CacheVimeoVideo(v)); 
            }
            catch
            {
                results = null;
            }

            return results; 
        }

        /// <summary>
        /// Get video from cache, otherwise request then add to cache
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        internal VimeoVideo GetVimeoVideo(long clipId)
        {
            VimeoVideo video = this.MemoryCache.Get(clipId.ToString()) as VimeoVideo;
            
            if (video == null)
            {
                try
                {
                    video = this.Vimeo.GetVideo(clipId);
                }
                catch
                {
                    video = null;
                }

                if (video != null)
                {
                    video = this.CacheVimeoVideo(video);    
                }                
            }

            return video;
        }

        internal VideoOEmbed GetVideoOEmbed(VideoOEmbedRequestSettings requestSettings)
        {
            return this.Vimeo.GetVideoOEmbedData(requestSettings); 
        }

        /// <summary>
        /// Add the VimeoVideo to the cache
        /// </summary>
        /// <param name="video">the vimeoVideo to be cached</param>
        /// <returns>the same vimeoVideo</returns>
        private VimeoVideo CacheVimeoVideo(VimeoVideo video)
        {
            this.MemoryCache.Set(
                new CacheItem(video.VideoId.ToString(), video),
                new CacheItemPolicy() { SlidingExpiration = new System.TimeSpan(0, 5, 0) });
               
            return video;
        }
    }
}
