﻿namespace nuPickers.VimeoPro
{
    using Newtonsoft.Json.Linq;
    using nuPickers.VimeoPro.Model;
    using nuPickers;
    using nuPickers.Shared.DotNetDataSource;
    using System.Collections.Generic;
    using System.Linq;

    public static class PickerExtensions
    {
        /// <summary>
        /// Query Vimeo to get a VimeoVideo object for each picked key
        /// </summary>
        /// <param name="picker">the nuPicker Picker</param>
        /// <returns>a collection of VimeoVideo objects</returns>
        public static IEnumerable<VimeoVideo> GetPickedVideos(this Picker picker)
        {
            var videos = new List<VimeoVideo>();

            foreach (string pickedKey in picker.PickedKeys)
            {
                videos.Add(picker.GetVimeoVideo(pickedKey));
            }

            return videos;
        }

        public static long GetVideoId(this Picker picker, string key)
        {
            long videoId;
            if (!long.TryParse(key, out videoId))
            {
                throw new System.ArgumentException("key must be a valid long value", "key");
            }
            return videoId; 
        }

        public static VimeoVideo GetVimeoVideo(this Picker picker, string key)
        {
            var videoId = picker.GetVideoId(key); 
            return GetVimeoConnection(picker).GetVimeoVideo(videoId);
        }

        public static VideoOEmbed GetVideoOEmbed(this Picker picker, string key)
        {
            return picker.GetVideoOEmbed(new VideoOEmbedRequestSettings(picker.GetVideoId(key))); 
        }

        public static VideoOEmbed GetVideoOEmbed(this Picker picker, VideoOEmbedRequestSettings requestSettings)
        {
            return GetVimeoConnection(picker).GetVideoOEmbed(requestSettings);
        }

        private static VimeoConnection GetVimeoConnection(Picker picker)
        {
            var dotNetDataSource = JObject.Parse(picker.GetDataTypePreValue("dataSource").Value).ToObject<DotNetDataSource>();
            string authToken = dotNetDataSource.Properties.Single(x => x.Name == "AuthKey").Value;
            return VimeoManager.GetVimeoConnection(authToken);
        }
    }
}
