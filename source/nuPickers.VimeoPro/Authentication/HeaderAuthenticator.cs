﻿namespace nuPickers.VimeoPro.Authentication
{
    using RestSharp;

    internal class HeaderAuthenticator : IAuthenticator
    {
        public string HeaderKey { get; set; }
        public string HeaderValue { get; set; }

        public HeaderAuthenticator(string headerKey, string headerValue)
        {
            this.HeaderKey = headerKey;
            this.HeaderValue = headerValue;
        }
        public void Authenticate(IRestClient client, IRestRequest request)
        {
            request.AddHeader(HeaderKey, HeaderValue);
        }
    }
}
