﻿namespace nuPickers.VimeoPro.Authentication
{
    internal class VimeoAuthenticator : HeaderAuthenticator
    {
        internal VimeoAuthenticator(string accessToken) :
            base("Authorization", string.Format("bearer {0}", accessToken))
        {
        }
    }
}

