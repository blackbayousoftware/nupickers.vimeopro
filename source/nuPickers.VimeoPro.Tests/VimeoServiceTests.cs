﻿namespace nuPickers.VimeoPro.Tests
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using nuPickers.VimeoPro;
    using nuPickers.VimeoPro.Model;
    using nuPickers.VimeoPro.Tests.Settings;

    [TestClass]
    public class VimeoServiceTests
    {
        private VimeoApiTestSettings vimeoSettings;
        private VimeoService service;

        [TestInitialize]
        public void SetupTest()
        {
            // Load the settings from a file that is not under version control for security
            // The settings loader will create this file in the bin/ folder if it doesn't exist
            vimeoSettings = Settings.SettingsLoader.LoadSettings();

            service = new VimeoService(vimeoSettings.AccessToken); 
        }

        [TestMethod]
        public void SearchForVideos()
        {
            var searchOptions = new VideoSearchOptions
            {
                UserId = vimeoSettings.UserId,
                Text = vimeoSettings.SearchText
            }; 

            var result = service.SearchVideos(searchOptions);
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count, vimeoSettings.TextSearchExpectedResults);
        }

        [TestMethod]
        public void GetOEmbedCode()
        {
            var result = service.GetVideoOEmbedData(new VideoOEmbedRequestSettings(vimeoSettings.OEmbedVideoId));

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Html);
        }
    }
}
