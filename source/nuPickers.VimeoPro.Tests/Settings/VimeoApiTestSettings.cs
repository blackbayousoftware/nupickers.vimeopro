﻿namespace nuPickers.VimeoPro.Tests.Settings
{
    internal class VimeoApiTestSettings
    {
        // API Client Settings
        public string AccessToken { get; set; }

        // Test Data Settings
        public string UserId { get; set; }
        public string SearchText { get; set; }
        public int TextSearchExpectedResults { get; set; }
        public long OEmbedVideoId { get; set; }
    }
}
